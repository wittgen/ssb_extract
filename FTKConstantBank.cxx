//#include "FTKSetup.h"
#include "FTKConstantBank.h"
//#include "TrigFTKSim/FTKTrack.h"
#include "ftk_dcap.h"

// #include <TMatrixD.h>
// #include <TVectorD.h>
//#include <TMath.h>
#include <Eigen/Dense>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cassert>
#include <cstdio>
#include <cmath>
#include <bitset>
using namespace std;

//#define SIMPLEMJ // ibl undefined simple majority to see if we can get majority for ibl
//#define SIMPLEMJ_DBG


FTKConstantBank::FTKConstantBank() :
  m_bankID(-1), m_nsectors(0), m_nplanes(0),
  m_npars(0), m_ncoords(0), m_AUX(false), 
  m_coordsmask(0), m_missid(0),
  m_nconstr(0), m_isgood(0),
  m_fit_pars(0), m_fit_const(0), m_kernel(0), m_kaverage(0),
  m_invfit_consts(0x0),
  m_maj_a(0), m_maj_kk(0), m_maj_invkk(0),
  m_kernel_aux(0), 
  m_kernel_hw(0), 
  m_kaverage_aux(0),
  m_maj_invkk_aux(0),
  m_maj_invkk_hw(0),
  m_maj_invkk_pow(0), m_maj_invkk_pow_hw(0)
{
  // nothing to do
}


FTKConstantBank::FTKConstantBank(int ncoords, const char *fname) :
  m_bankID(0),
  m_nplanes(0),
  m_AUX(false),
  m_invfit_consts(0x0),
  m_kernel_aux(0),
  m_kernel_hw(0),
  m_kaverage_aux(0),
  m_maj_invkk_aux(0),
  m_maj_invkk_hw(0),
  m_maj_invkk_pow(0),
  m_maj_invkk_pow_hw(0)
{
  //TODO: make possible to read constants in different formats
  m_ncoords = ncoords;
  m_coordsmask = new int[m_ncoords];
  m_missid = new int[m_ncoords];

   // if boost::iostreams is available, use it to give C++ iostreams
   // the ability to read from dCache, gzip, and/or bzip2 sources.
   // otherwise, stick with Guido's implementation and read text
   // files straight from a regular filesystem.
  ftk_dcap::istream geocfile;
  const bool ok_read = ftk_dcap::open_for_read(fname,geocfile);
  if (!geocfile || !ok_read) {
    //    FTKSetup::PrintMessageFmt(ftk::sevr,"Constants' file: %s invalid\n",
    //			      fname);
  }

  string line;
  for (int i=0;i<5;++i) // skip 5 lines
    getline(geocfile,line);
  
  string key;
  int ival;

  geocfile >> key;
  if (key!="NPLANES") {
    //    FTKSetup::PrintMessageFmt(ftk::sevr,"Invalid file format in: %s\n",
    //			      fname);
  }
  geocfile >> ival;

  geocfile >> key;
  if (key!="NSECTORS") {
    //    FTKSetup::PrintMessageFmt(ftk::sevr,"Invalid file format in: %s\n",
    //			      fname);
  }
  geocfile >> m_nsectors;

  geocfile >> key;
  if (key!="NDIM") {
    // FTKSetup::PrintMessageFmt(ftk::sevr,"Invalid file format in: %s\n",
    //			      fname);
  }
  geocfile >> ival;
  if (ival==1)
    m_npars = 3;
  else if (ival==2)
    m_npars = 5;
  //else 
    //    FTKSetup::PrintMessage(ftk::sevr,"Number of dimension invalid");

  // fix the constraints number
  m_nconstr = m_ncoords-m_npars;

  // allocate the block of pointer per sector
  m_isgood = new bool[m_nsectors];
  m_fit_pars = new float**[m_nsectors];
  m_fit_const = new float*[m_nsectors];
  m_kernel = new float**[m_nsectors];
  m_kaverage = new float*[m_nsectors];

  cout<<"FTKConstantBank settings: m_ncoords="<<m_ncoords<<" m_npars="<<m_npars<<"\n";

  int isec_step = 1 + m_nsectors/10;
  for (int isec=0;isec<m_nsectors;++isec) { // loop over sectors

    double dval; // tmp value used to conver from double to float

    if ((isec%isec_step == 0) /* ||(isec<10) */)
      cout << "Load sector: " << isec << "/" << m_nsectors << std::endl;

    // check variable initilized to "false"
    m_isgood[isec] = false;

    geocfile >> key >> ival;
    /* if(key!="sector") {
       cerr << "expect key=\"sector\" found key=\""<<key<<"\"\n";
       } */

    if (ival!=isec) {
      cerr << "Attempt to read sector # " << ival << " (" << isec << ")" << std::endl;
      //      FTKSetup::PrintMessage(ftk::sevr,"Lost sync in constants file");
      assert(ival-isec);
    }
    
    // allocate space for constants in this sector
    m_fit_pars[isec] = new float*[m_npars];
    m_fit_const[isec] = new float[m_npars];
    for (int i=0;i<m_npars;++i) {
      m_fit_pars[isec][i] = new float[m_ncoords];
    }
    
    m_kernel[isec] = new float*[m_nconstr];
    m_kaverage[isec] = new float[m_nconstr];
    for (int i=0;i<m_nconstr;++i) {
      m_kernel[isec][i] = new float[m_ncoords];
    }

    for (int ipar=0;ipar<m_npars;++ipar) { // pars loop
      geocfile >> key;
      for (int icoord=0;icoord<m_ncoords;++icoord) { // coords loop
        geocfile >> dval;
        if(geocfile.fail()) {
	  //           FTKSetup::PrintMessageFmt
	  //  (ftk::sevr,"par loop (1) key=\"%s\" ipar,icoord=%d,%d\n",
	  //   key.c_str(),ipar,icoord);
        }
        //cout<<dval<<"\n";
        if (dval!=0.) m_isgood[isec] = true;
        m_fit_pars[isec][ipar][icoord] = dval;
      } // end coords loop
    } // end pars loop


    geocfile >> key;
    assert(key=="kaverages");
    for (int ik=0;ik<m_nconstr;++ik) { // loop kaverages
      geocfile >> dval;
      if (dval!=0.) m_isgood[isec] = true;
      m_kaverage[isec][ik] = dval;
    } // end loop kaverages

    geocfile >> key;
    assert(key=="kernel");
    for (int ik=0;ik<m_nconstr;++ik) { // loop kaverages
      for (int ik2=0;ik2<m_ncoords;++ik2) { // loop kaverages
        geocfile >> dval;
        if (dval!=0.) m_isgood[isec] = true;
        m_kernel[isec][ik][ik2] = dval;
      }
    } // end loop kaverages

    for (int ipar=0;ipar<m_npars;++ipar) { // pars loop
      geocfile >> key;
      //cout<<"second pars loop key="<<key<<"\n";
      geocfile >> dval;
      if (dval!=0.) m_isgood[isec] = true;
      m_fit_const[isec][ipar] = dval;
    } // end pars loop

  } // end loop over sectors

  // pre-calculate the majority logic elements
  m_maj_a = new double*[m_nsectors];
  m_maj_kk = new double**[m_nsectors];
  m_maj_invkk = new double**[m_nsectors];
  for (int isec=0;isec!=m_nsectors;++isec) { // sector loop
    m_maj_a[isec] = new double[m_ncoords];
    m_maj_kk[isec] = new double*[m_ncoords];
    m_maj_invkk[isec] = new double*[m_ncoords];
    for (int ix=0;ix!=m_ncoords;++ix) { // 1st coordinate loop    
      m_maj_a[isec][ix] = 0.;
      for (int row=0;row!=m_nconstr;++row) {
      	m_maj_a[isec][ix] += m_kaverage[isec][row]*m_kernel[isec][row][ix];
      }

      m_maj_kk[isec][ix] = new double[m_ncoords];
      m_maj_invkk[isec][ix] = new double[m_ncoords];
      for (int jx=0;jx!=m_ncoords;++jx) { // 2nd coordinate loop
        m_maj_invkk[isec][ix][jx] = m_maj_kk[isec][ix][jx] = .0;
      	for (int row=0;row!=m_nconstr;++row) {
          m_maj_kk[isec][ix][jx] += m_kernel[isec][row][ix]*m_kernel[isec][row][jx];
      	}
      } // end 2nd coordinate loop
    } // end 1st coordinate loop

    // here the m_maj_kk[isec] elements are filled and the inverse element,
    // used after to simplify the guessing algorithm, can be computed
    // int npixcy = FTKSetup::getFTKSetup().getIBLMode()==1 ? 8 : 6; // this is for ibl, to know how many pixel coords there are
    int npixcy = 6; // we have 6 pixel coords -- not sure what to do about this!!
    /* PIXEL layer [0-5]*/
    for (int ix=0;ix!=npixcy;ix+=2) { // pxl layers loop (2 coordinates each)
      double det =   m_maj_kk[isec][ix][ix]   * m_maj_kk[isec][ix+1][ix+1]
                   - m_maj_kk[isec][ix+1][ix] * m_maj_kk[isec][ix][ix+1];
      m_maj_invkk[isec][ix][ix] = m_maj_kk[isec][ix+1][ix+1]/det;
      m_maj_invkk[isec][ix][ix+1] = -m_maj_kk[isec][ix+1][ix]/det;
      m_maj_invkk[isec][ix+1][ix] = -m_maj_kk[isec][ix][ix+1]/det;
      m_maj_invkk[isec][ix+1][ix+1] = m_maj_kk[isec][ix][ix]/det;

    } // end pxl layers loop
    /* SCT layers */
    for (int ix=npixcy;ix!=m_ncoords;++ix) { // SCT layers loop (1 coordinate each)
      m_maj_invkk[isec][ix][ix] = 1./m_maj_kk[isec][ix][ix];      
    } // end SCT layers loop

  } // end sector loop
}

void FTKConstantBank::doAuxFW(bool do_it) {

  m_AUX = do_it;
  if (!do_it) return;
  if (m_ncoords == 16) return;

  m_kernel_aux = new signed long long**[m_nsectors];
  m_kernel_hw = new signed long long**[m_nsectors];
  m_kaverage_aux = new signed long long*[m_nsectors];

  for (int isec=0;isec<m_nsectors;++isec) { // loop over sectors
    
    m_kernel_aux[isec] = new signed long long*[m_nconstr];
    m_kernel_hw[isec] = new signed long long*[m_nconstr];
    m_kaverage_aux[isec] = new signed long long[m_nconstr];
    for (int i=0;i<m_nconstr;++i) {
      m_kernel_aux[isec][i] = new signed long long[m_ncoords];
      m_kernel_hw[isec][i] = new signed long long[m_ncoords];
    }

    bool oflAUX = false, oflHW = false; // overflow for aux shifts.
    for (int ik=0;ik<m_nconstr;++ik) { // loop kaverages
      m_kaverage_aux[isec][ik] = aux_asr(m_kaverage[isec][ik] * pow(2., KAVE_SHIFT), 0, FIT_PREC, oflAUX); 
      m_kaverage_aux[isec][ik] = aux_asr(m_kaverage[isec][ik] * pow(2., KAVE_SHIFT), 0, FIT_PREC, oflHW); 
    } // end loop kaverages
    //    if (oflAUX) FTKSetup::PrintMessageFmt(ftk::warn, "  AUX kaverage overflowed allowed precision in sector %d.\n", isec);
    //    if (oflHW)  FTKSetup::PrintMessageFmt(ftk::warn, "  HW  kaverage overflowed allowed precision in sector %d.\n", isec);

    oflAUX = oflHW = false;
    for (int ik=0;ik<m_nconstr;++ik) { // loop kaverages
      for (int ik2=0;ik2<m_ncoords;++ik2) { // loop kaverages
        m_kernel_aux[isec][ik][ik2] = aux_asr(m_kernel[isec][ik][ik2] * pow(2., KERN_SHIFT), 0, FIT_PREC, oflAUX); 
        m_kernel_hw[isec][ik][ik2]  = aux_asr(const_scale_map[ik2] * m_kernel[isec][ik][ik2] * pow(2., KERN_SHIFT_HW), 0, 50, oflHW); // FIT_PREC
      }
    } // end loop kaverages

    //    if (oflAUX) FTKSetup::PrintMessageFmt(ftk::warn, "  AUX kernel overflowed allowed precision in sector %d.\n", isec);
    //    if (oflHW)  FTKSetup::PrintMessageFmt(ftk::warn, "  HW  kernel overflowed allowed precision in sector %d.\n", isec);
  
  } // end loop over sectors



  // pre-calculate the majority logic elements
  m_maj_invkk_hw = new signed long long**[m_nsectors];
  m_maj_invkk_aux = new signed long long**[m_nsectors];
  m_maj_invkk_pow = new short int**[m_nsectors];
  m_maj_invkk_pow_hw = new short int**[m_nsectors];
  for (int isec=0;isec!=m_nsectors;++isec) { // sector loop
    m_maj_invkk_hw [isec] = new signed long long*[m_ncoords];
    m_maj_invkk_aux[isec] = new signed long long*[m_ncoords];
    m_maj_invkk_pow[isec] = new short int*[m_ncoords];
    m_maj_invkk_pow_hw[isec] = new short int*[m_ncoords];
    for (int ix=0;ix!=m_ncoords;++ix) { // 1st coordinate loop    

      m_maj_invkk_hw [isec][ix] = new signed long long[m_ncoords];
      m_maj_invkk_aux[isec][ix] = new signed long long[m_ncoords];
      m_maj_invkk_pow[isec][ix] = new short int[m_ncoords];
      m_maj_invkk_pow_hw[isec][ix] = new short int[m_ncoords];
      for (int jx=0;jx!=m_ncoords;++jx) { // 2nd coordinate loop
        m_maj_invkk_hw [isec][ix][jx] = 0;
        m_maj_invkk_aux[isec][ix][jx] = 0;
        m_maj_invkk_pow[isec][ix][jx] = 0;
        m_maj_invkk_pow_hw[isec][ix][jx] = 0;
      	for (int row=0;row!=m_nconstr;++row) {
          m_maj_kk[isec][ix][jx] += m_kernel[isec][row][ix]*m_kernel[isec][row][jx];
      	}
      } // end 2nd coordinate loop
    } // end 1st coordinate loop

    // here the m_maj_kk[isec] elements are filled and the inverse element,
    // used after to simplify the guessing algorithm, can be computed
    // int npixcy = FTKSetup::getFTKSetup().getIBLMode()==1 ? 8 : 6; // this is for ibl, to know how many pixel coords there are
    int npixcy = 6; // we have 6 pixel coords -- not sure what to do about this!!
    /* PIXEL layer [0-5]*/
    bool ofl = false;
    for (int ix=0;ix!=npixcy;ix+=2) { // pxl layers loop (2 coordinates each)

      m_maj_invkk_pow[isec][ix][ix]        = CONST_PREC - std::ceil(std::log2(fabs(m_maj_invkk[isec][ix][ix]    ))) - 1;
      m_maj_invkk_pow[isec][ix][ix+1]      = CONST_PREC - std::ceil(std::log2(fabs(m_maj_invkk[isec][ix][ix+1]  ))) - 1;
      m_maj_invkk_pow[isec][ix+1][ix]      = CONST_PREC - std::ceil(std::log2(fabs(m_maj_invkk[isec][ix+1][ix]  ))) - 1;
      m_maj_invkk_pow[isec][ix+1][ix+1]    = CONST_PREC - std::ceil(std::log2(fabs(m_maj_invkk[isec][ix+1][ix+1]))) - 1;

      m_maj_invkk_pow_hw[isec][ix][ix]     = CONST_PREC - std::ceil(std::log2(fabs(8./(const_scale_map[ix]   * const_scale_map[ix]  ) * m_maj_invkk[isec][ix][ix]    ))) - 1;
      m_maj_invkk_pow_hw[isec][ix][ix+1]   = CONST_PREC - std::ceil(std::log2(fabs(8./(const_scale_map[ix]   * const_scale_map[ix+1]) * m_maj_invkk[isec][ix][ix+1]  ))) - 1;
      m_maj_invkk_pow_hw[isec][ix+1][ix]   = CONST_PREC - std::ceil(std::log2(fabs(8./(const_scale_map[ix+1] * const_scale_map[ix]  ) * m_maj_invkk[isec][ix+1][ix]  ))) - 1;
      m_maj_invkk_pow_hw[isec][ix+1][ix+1] = CONST_PREC - std::ceil(std::log2(fabs(8./(const_scale_map[ix+1] * const_scale_map[ix+1]) * m_maj_invkk[isec][ix+1][ix+1]))) - 1;


      m_maj_invkk_aux[isec][ix][ix]     = aux_asr(m_maj_invkk[isec][ix][ix]     * pow(2, m_maj_invkk_pow[isec][ix][ix]),     0, CONST_PREC, ofl);
      m_maj_invkk_aux[isec][ix][ix+1]   = aux_asr(m_maj_invkk[isec][ix][ix+1]   * pow(2, m_maj_invkk_pow[isec][ix][ix+1]),   0, CONST_PREC, ofl);
      m_maj_invkk_aux[isec][ix+1][ix]   = aux_asr(m_maj_invkk[isec][ix+1][ix]   * pow(2, m_maj_invkk_pow[isec][ix+1][ix]),   0, CONST_PREC, ofl);
      m_maj_invkk_aux[isec][ix+1][ix+1] = aux_asr(m_maj_invkk[isec][ix+1][ix+1] * pow(2, m_maj_invkk_pow[isec][ix+1][ix+1]), 0, CONST_PREC, ofl);

      m_maj_invkk_hw [isec][ix][ix]     = aux_asr(8./(const_scale_map[ix]   * const_scale_map[ix]  ) * m_maj_invkk[isec][ix][ix]     * pow(2, m_maj_invkk_pow_hw[isec][ix][ix]),     0, CONST_PREC, ofl);
      m_maj_invkk_hw [isec][ix][ix+1]   = aux_asr(8./(const_scale_map[ix]   * const_scale_map[ix+1]) * m_maj_invkk[isec][ix][ix+1]   * pow(2, m_maj_invkk_pow_hw[isec][ix][ix+1]),   0, CONST_PREC, ofl);
      m_maj_invkk_hw [isec][ix+1][ix]   = aux_asr(8./(const_scale_map[ix+1] * const_scale_map[ix]  ) * m_maj_invkk[isec][ix+1][ix]   * pow(2, m_maj_invkk_pow_hw[isec][ix+1][ix]),   0, CONST_PREC, ofl);
      m_maj_invkk_hw [isec][ix+1][ix+1] = aux_asr(8./(const_scale_map[ix+1] * const_scale_map[ix+1]) * m_maj_invkk[isec][ix+1][ix+1] * pow(2, m_maj_invkk_pow_hw[isec][ix+1][ix+1]), 0, CONST_PREC, ofl);

    } // end pxl layers loop
    /* SCT layers */
    for (int ix=npixcy;ix!=m_ncoords;++ix) { // SCT layers loop (1 coordinate each)
      m_maj_invkk[isec][ix][ix] = 1./m_maj_kk[isec][ix][ix];      

      m_maj_invkk_pow[isec][ix][ix] = CONST_PREC - std::ceil(std::log2(fabs(m_maj_invkk[isec][ix][ix]))) - 1;
      m_maj_invkk_aux[isec][ix][ix] = aux_asr(m_maj_invkk[isec][ix][ix] * pow(2, m_maj_invkk_pow[isec][ix][ix]), 0, CONST_PREC, ofl);

      m_maj_invkk_pow_hw[isec][ix][ix] = CONST_PREC - std::ceil(std::log2(fabs(8./(const_scale_map[ix] * const_scale_map[ix]) * m_maj_invkk[isec][ix][ix]))) - 1;
      m_maj_invkk_hw    [isec][ix][ix] = aux_asr(8./(const_scale_map[ix] * const_scale_map[ix]) * m_maj_invkk[isec][ix][ix] * pow(2, m_maj_invkk_pow_hw[isec][ix][ix]), 0, CONST_PREC, ofl);
    } // end SCT layers loop

    //    if (ofl) FTKSetup::PrintMessageFmt(ftk::warn, "  maj/invkk overflowed allowed precision in sector %d.\n", isec);

  } // end sector loop

}

        

FTKConstantBank::~FTKConstantBank()
{
  if (m_nsectors) {
    for (int isec=0;isec<m_nsectors;++isec) {
      for (int ipar=0;ipar<m_npars;++ipar) {
      	delete [] m_fit_pars[isec][ipar];
      }
      delete [] m_fit_pars[isec];
      delete [] m_fit_const[isec];
      for (int icoord=0;icoord<m_nconstr;++icoord) {
      	delete [] m_kernel[isec][icoord];
      	if (m_kernel_aux) delete [] m_kernel_aux[isec][icoord];
      	if (m_kernel_hw) delete [] m_kernel_hw[isec][icoord];
      }
      delete [] m_kernel[isec];
      delete [] m_kaverage[isec];
      if (m_kernel_aux) delete [] m_kernel_aux[isec];
      if (m_kernel_hw) delete [] m_kernel_hw[isec];
      if (m_kaverage_aux) delete [] m_kaverage_aux[isec];

      for (int ix=0;ix!=m_ncoords;++ix) {
        if (m_maj_kk) delete [] m_maj_kk[isec][ix];
        if (m_maj_invkk_hw) delete [] m_maj_invkk_hw[isec][ix];
        if (m_maj_invkk_aux) delete [] m_maj_invkk_aux[isec][ix];
        if (m_maj_invkk_pow) delete [] m_maj_invkk_pow[isec][ix];
        if (m_maj_invkk_pow_hw) delete [] m_maj_invkk_pow_hw[isec][ix];
      }
    }

    delete [] m_fit_pars;
    delete [] m_fit_const;
    delete [] m_kernel;
    delete [] m_kaverage;
    if (m_kernel_aux) delete [] m_kernel_aux;
    if (m_kernel_hw) delete [] m_kernel_hw;
    if (m_kaverage_aux) delete [] m_kaverage_aux;

    delete [] m_maj_a;
    delete [] m_maj_kk;
    if (m_maj_invkk_hw) delete [] m_maj_invkk_hw;
    if (m_maj_invkk_aux) delete [] m_maj_invkk_aux;
    if (m_maj_invkk_pow) delete [] m_maj_invkk_pow;
    if (m_maj_invkk_pow_hw) delete [] m_maj_invkk_pow_hw;
    delete [] m_maj_invkk;
    delete [] m_isgood;
  }

  delete [] m_coordsmask;
  delete [] m_missid;
}



unsigned int FTKConstantBank::floatToReg27(float f) {

  int f_f = (*(int*)&f);
  int f_sign = (f_f >> 31) & 0x1;
  int f_exp = (f_f >> 23) & 0xFF;
  int f_frac = f_f & 0x007FFFFF;
  int r_sign;
  int r_exp;
  int r_frac;
  r_sign = f_sign;
  if((f_exp == 0x00) || (f_exp == 0xFF)) {
    // 0x00 -> 0 or subnormal
    // 0xFF -> infinity or NaN
    r_exp = 0;
    r_frac = 0;
  } else {
    r_exp = (f_exp) & 0xFF;
    r_frac = ((f_frac >> 5)) & 0x0003FFFF;
  }
  return (r_sign << 26) | (r_exp << 18) | r_frac;
}

Eigen::MatrixXd FTKConstantBank::get_A_matrix(
					      int secid,
					      std::vector<int> real_idx,
					      std::vector<int> miss_idx,
					      std::vector<double> hw_scale
					      )
{

  // instantitae A_matrix
  Eigen::MatrixXd A_matrix(11, miss_idx.size()); // 11 will never change for FTK system (this is 16 coordinates minus 5 helix parameters)

  // Defining A column from the "m_kernel"
  for (unsigned int icol = 0; icol < miss_idx.size(); ++icol)
    {
      for (unsigned int irow = 0; irow < 11; ++irow)
	{
	  // obtain the A vector in ideal coordinates
	  double A_irow_icol = m_kernel[secid][irow][miss_idx[icol]];

	  // scale them to hw_scale
	  A_irow_icol /= hw_scale.at(miss_idx[icol]);

	  // set the A matrix
	  A_matrix(irow, icol) = A_irow_icol;
	}
    }

  return A_matrix;
}
//======================
// B column vectors = B matrix
//======================
Eigen::MatrixXd FTKConstantBank::get_B_matrix(
					      int secid,
					      std::vector<int> real_idx,
					      std::vector<int> miss_idx,
					      std::vector<double> hw_scale
					      )
{
  // instantiate B_matrix, very similar to A_matrix case
  Eigen::MatrixXd B_matrix(11, real_idx.size()); // 11 will never change for FTK system (this is 16 coordinates minus 5 helix parameters)

  // set the values
  for (unsigned int icol = 0; icol < real_idx.size(); ++icol)
    {
      for (unsigned int irow = 0; irow < 11; ++irow)
	{
	  double B_irow_icol = m_kernel[secid][irow][real_idx[icol]];

	  B_irow_icol /= hw_scale.at(real_idx[icol]);

	  B_matrix(irow, icol) = B_irow_icol;
	}
    }

  return B_matrix;
}
//======================
// C matrix
//======================
Eigen::MatrixXd FTKConstantBank::get_C_matrix(
					      Eigen::MatrixXd A_matrix
					      )
{
  // the number of column vectors will be the row and col dim of C_matrix
  int col_dim_A = A_matrix.cols();

  // create C_matrix
  Eigen::MatrixXd C_matrix(col_dim_A, col_dim_A);

  // set the values;
  for (int i = 0; i < col_dim_A; ++i)
    {
      for (int j = 0; j < col_dim_A; ++j)
	{
	  C_matrix(i, j) = A_matrix.col(i).transpose() * A_matrix.col(j);
	}
    }

  return C_matrix;
}




//======================
// h vector
//======================
// The h vectors
Eigen::VectorXd FTKConstantBank::get_h_vector(
					      int secid,
					      std::vector<int> real_idx
					      )
{
  // h_vector has dimension of number of real hits
  Eigen::VectorXd h_vector(real_idx.size());

  // set the values
  for (unsigned int i = 0; i < real_idx.size(); ++i)
    {
      h_vector(i) = m_kaverage[secid][i];
    }

  return h_vector;
}


//======================
// J vector
//======================
Eigen::VectorXd FTKConstantBank::get_J_vector(
					      Eigen::MatrixXd A_matrix,
					      Eigen::VectorXd h_vector
					      )
{
  // J_vector has the dimension of missed coordinates
  // (which is same as columns in A_matrix)
  Eigen::VectorXd J_vector(A_matrix.cols());

  // set the values
  for (unsigned int i = 0; i < A_matrix.cols(); ++i)
    {
      J_vector(i) = A_matrix.col(i).transpose() * h_vector;
    }

  return J_vector;
}




//======================
// D matrix
//======================
Eigen::MatrixXd FTKConstantBank::get_D_matrix(
					      Eigen::MatrixXd A_matrix,
					      Eigen::MatrixXd B_matrix
					      )
{
  // instantitaion
  Eigen::MatrixXd D_matrix(A_matrix.cols(), B_matrix.cols());

  // set the value
  for (unsigned int i = 0; i < A_matrix.cols(); ++i)
    {
      for (unsigned int j = 0; j < B_matrix.cols(); ++j)
	{
	  D_matrix(i, j) = A_matrix.col(i).transpose() * B_matrix.col(j);
	}
    }

  return D_matrix;
}


//======================
// F vector
//======================
Eigen::VectorXd FTKConstantBank::get_F_vector(
					      Eigen::MatrixXd C_matrix,
					      Eigen::VectorXd J_vector
					      )
{
  // instantiation
  Eigen::VectorXd F_vector(C_matrix.cols());

  F_vector = C_matrix.inverse() * J_vector * -1;

  return F_vector;
}
//==============================================
// E_matrix
//==============================================
Eigen::MatrixXd FTKConstantBank::get_E_matrix(
    Eigen::MatrixXd C_matrix,
    Eigen::MatrixXd D_matrix
)
{
  // instantitaion
  Eigen::MatrixXd E_matrix(C_matrix.cols(), D_matrix.cols());

  // calculate
  E_matrix = C_matrix.inverse() * D_matrix * -1.;

  return E_matrix;
}
unsigned int FTKConstantBank::createMask(unsigned int a, unsigned int b)
{
   unsigned int r = 0;
   for (unsigned int i=a; i<=b; i++)
       r |= 1 << i;

   return r;
}


void FTKConstantBank::calcExtrapolationConstant(int secid, vector<int> moduleid, int eightl_secid, int nconn, int sizenconn , std::vector<uint32_t> &out){



  int nmissing = 0;
  //int i;
  // evaluate the number of missings points
  for (int i=0;i<m_ncoords;++i) {
    m_missid[i] = -1;
    if (!m_coordsmask[i]) {
      m_missid[nmissing] = i; // rec the index of missing
      nmissing++;
    }
  
}
  std::vector<double> hw_scale;

  hw_scale.push_back(8.);  
  hw_scale.push_back(16.32); 
  hw_scale.push_back(8.);   
  hw_scale.push_back(19./18. * 16.);
  hw_scale.push_back(8.);
  hw_scale.push_back(19./18. * 16.);
  hw_scale.push_back(8.);
  hw_scale.push_back(19./18. * 16.);
  hw_scale.push_back(2.);
  hw_scale.push_back(2.);
  hw_scale.push_back(2.);
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.);                                                           
                       

  std::vector<int> real_idx;
  std::vector<int> miss_idx;
  miss_idx.push_back(0);
  miss_idx.push_back(1);
  miss_idx.push_back(9);
  miss_idx.push_back(11);
  miss_idx.push_back(15);

  real_idx.push_back(2);
  real_idx.push_back(3);
  real_idx.push_back(4);
  real_idx.push_back(5);
  real_idx.push_back(6);
  real_idx.push_back(7);
  real_idx.push_back(8);
  real_idx.push_back(10);
  real_idx.push_back(12);
  real_idx.push_back(13);
  real_idx.push_back(14);

  Eigen::MatrixXd A = get_A_matrix(secid,real_idx,miss_idx,hw_scale);
  Eigen::MatrixXd B = get_B_matrix(secid,real_idx,miss_idx,hw_scale);
  Eigen::MatrixXd C = get_C_matrix(A);
  Eigen::MatrixXd D = get_D_matrix(A,B);
  Eigen::VectorXd h = get_h_vector(secid,real_idx);
  Eigen::VectorXd J = get_J_vector(A,h);
  Eigen::MatrixXd E = get_E_matrix(C,D);
  Eigen::VectorXd F = get_F_vector(C,J);



  vector<double> Constants;
  Constants.clear();

  Constants.push_back(F[0]);
  Constants.push_back(E(0,0));
  Constants.push_back(E(0,1));
  Constants.push_back(E(0,2));
  Constants.push_back(E(0,3));
  Constants.push_back(E(0,4));
  Constants.push_back(E(0,5));
  Constants.push_back(E(0,6));
  Constants.push_back(E(0,7));
  Constants.push_back(E(0,8));
  Constants.push_back(E(0,9));
  Constants.push_back(E(0,10));
  
  Constants.push_back(F[1]);
  Constants.push_back(E(1,0));
  Constants.push_back(E(1,1));
  Constants.push_back(E(1,2));
  Constants.push_back(E(1,3));
  Constants.push_back(E(1,4));
  Constants.push_back(E(1,5));
  Constants.push_back(E(1,6));
  Constants.push_back(E(1,7));
  Constants.push_back(E(1,8));
  Constants.push_back(E(1,9));
  Constants.push_back(E(1,10));
  
  Constants.push_back(F[2]);
  Constants.push_back(E(2,0));
  Constants.push_back(E(2,1));
  Constants.push_back(E(2,2));
  Constants.push_back(E(2,3));
  Constants.push_back(E(2,4));
  Constants.push_back(E(2,5));
  Constants.push_back(E(2,6));
  Constants.push_back(E(2,7));
  Constants.push_back(E(2,8));
  Constants.push_back(E(2,9));
  Constants.push_back(E(2,10));
  

    Constants.push_back(F[3]);
    Constants.push_back(E(3,0));
    Constants.push_back(E(3,1));
    Constants.push_back(E(3,2));
    Constants.push_back(E(3,3));
    Constants.push_back(E(3,4));
    Constants.push_back(E(3,5));
    Constants.push_back(E(3,6));
    Constants.push_back(E(3,7));
    Constants.push_back(E(3,8));
    Constants.push_back(E(3,9));
    Constants.push_back(E(3,10));


    Constants.push_back(F[4]);
    Constants.push_back(E(4,0));
    Constants.push_back(E(4,1));
    Constants.push_back(E(4,2));
    Constants.push_back(E(4,3));
    Constants.push_back(E(4,4));
    Constants.push_back(E(4,5));
    Constants.push_back(E(4,6));
    Constants.push_back(E(4,7));
    Constants.push_back(E(4,8));
    Constants.push_back(E(4,9));
    Constants.push_back(E(4,10));


    int NconnID = nconn ;
    unsigned int word = (eightl_secid << 2) | NconnID ;
   

    out.push_back(word);

    for(int y=0;y<=8;y++){
      int x =y+y;
      unsigned mask =  (((1 << 2) - 1) << (x));
      word = ((moduleid[0] & mask) << (28-x)) | floatToReg27(Constants[y]);
       out.push_back(word);
    }
    word = floatToReg27(Constants[9]);
    out.push_back(word);
    for(int y=10;y<=18;y++){
      int x =(y+y-20);
      unsigned mask =  (((1 << 2) - 1) << x)  ;
      word = ((moduleid[1] & mask) << (28-x)) | floatToReg27(Constants[y]);
       out.push_back(word);
    }
    word = floatToReg27(Constants[19]);
    out.push_back(word);
    for(int y=20;y<=28;y++){
      int x =(y+y-40);
      unsigned mask =  (((1 << 2) - 1) << x);
      word = ((moduleid[2] & mask) << (28-x)) | floatToReg27(Constants[y]);
       out.push_back(word);
    }
    word = floatToReg27(Constants[29]);
    out.push_back(word);
    for(int y=30;y<=38;y++){
      int x =(y+y-60);
      unsigned mask =  (((1 << 2) - 1) << x);
      word = ((moduleid[3] & mask) << (28-x)) | floatToReg27(Constants[y]);
      out.push_back(word);
    }
    word = floatToReg27(Constants[39]);
    out.push_back(word);
    for(int y=40;y<=48;y++){
      int x =(y+y-80);
      unsigned mask =  (((1 << 2) - 1) << x) ;
      word = ((secid  & mask) << (28-x)) | floatToReg27(Constants[y]);
      out.push_back(word);
    }
    word = floatToReg27(Constants[49]);
     out.push_back(word);
    unsigned mask =  ((1 << 2) - 1) << 0;
    word = ((sizenconn & mask)<< (28)) | floatToReg27(Constants[50]);
    out.push_back(word);
    mask =  ((1 << 2) - 1) << 2;
    word = ((sizenconn & mask) << 26)  | floatToReg27(Constants[51]);
    out.push_back(word);
    word = floatToReg27(Constants[52]);
    out.push_back(word);
    word = floatToReg27(Constants[54]);
    out.push_back(word);
    word = floatToReg27(Constants[55]);
   out.push_back(word);
    word = floatToReg27(Constants[56]);
   out.push_back(word);
    word = floatToReg27(Constants[57]);
   out.push_back(word);
    word = floatToReg27(Constants[58]);
   out.push_back(word);
    word = floatToReg27(Constants[59]);
   out.push_back(word);
   out.push_back(0);
   out.push_back(0);
   out.push_back(0);

    return;

}
void FTKConstantBank::writeConstants( std::vector<uint32_t> &out,std::ofstream& myfile)
{
 myfile << internal // fill between the prefix and the number
	<< setfill('0'); // fill with 0
 for(auto o:out) {
   myfile << hex<< setw(8) << o << std::endl; 
 }
}

void FTKConstantBank::calcTFConstant(int secid, std::vector<uint32_t> &out){

  std::vector<double> hw_scale;
  //scaling for hw coordinates
  hw_scale.push_back(8.);  
  hw_scale.push_back(16.32); 
  hw_scale.push_back(8.);   
  hw_scale.push_back(19./18. * 16.);
  hw_scale.push_back(8.);
  hw_scale.push_back(19./18. * 16.);
  hw_scale.push_back(8.);
  hw_scale.push_back(19./18. * 16.);
  hw_scale.push_back(2.);
  hw_scale.push_back(2.);
  hw_scale.push_back(2.);
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.); 
  hw_scale.push_back(2.);                                                           


  std::vector<int> real_idx;

  real_idx.push_back(2);
  real_idx.push_back(3);
  real_idx.push_back(4);
  real_idx.push_back(5);
  real_idx.push_back(6);
  real_idx.push_back(7);
  real_idx.push_back(8);
  real_idx.push_back(10);
  real_idx.push_back(12);
  real_idx.push_back(13);
  real_idx.push_back(14);

  vector<Eigen::MatrixXd> Cinverse;


  for(int pp=0; pp<=11;pp++){
    std::vector<int> miss_idx1;                       
    miss_idx1.clear();
   if(pp ==0){
      miss_idx1.push_back(0);
      miss_idx1.push_back(1);
    }
    else if (pp ==1){
      miss_idx1.push_back(2);
      miss_idx1.push_back(3);
    }
    else if (pp ==2){
      miss_idx1.push_back(4);
      miss_idx1.push_back(5);
    }
    else if (pp ==3){
      miss_idx1.push_back(6);
      miss_idx1.push_back(7);
    }else{
      miss_idx1.push_back(pp+4);
    }


    Eigen::MatrixXd A = get_A_matrix(secid,real_idx,miss_idx1,hw_scale);//11 (column)x16 (row) 
    Eigen::MatrixXd C = get_C_matrix(A);
    Eigen::MatrixXd Cinverse_temp(C.rows(),C.cols());
    Cinverse_temp = C.inverse();
    Cinverse.push_back(Cinverse_temp);
  }
 
  std::vector<int> miss_idx;
  miss_idx.push_back(0);
  miss_idx.push_back(1);
  miss_idx.push_back(2);
  miss_idx.push_back(3);
  miss_idx.push_back(4);
  miss_idx.push_back(5);
  miss_idx.push_back(6);
  miss_idx.push_back(7);
  miss_idx.push_back(8);
  miss_idx.push_back(9);
  miss_idx.push_back(10);
  miss_idx.push_back(11);
  miss_idx.push_back(12);
  miss_idx.push_back(13);
  miss_idx.push_back(14);
  miss_idx.push_back(15);


  int start = m_ncoords-1; 
  int finish =0;
  int number_block_transfer =0;
  unsigned int parola = (secid<<4) | number_block_transfer ; 
  
  
  out.push_back(parola);
  out.push_back(0); 
  out.push_back(floatToReg27(Cinverse[0](1,0))); //missing pix0 constants
  out.push_back(floatToReg27(Cinverse[0](0,0))); //missing pix0 constants
  out.push_back(floatToReg27(m_kaverage[secid][0])); 
  for (int ik2=start;ik2>=finish;--ik2) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][0][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[0](1,1))); //missing pix0 constants
  out.push_back(floatToReg27(Cinverse[0](0,1))); //missing pix0 constants
  out.push_back(floatToReg27(m_kaverage[secid][1])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][1][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[1](1,0))); //missing pix1 constants
  out.push_back(floatToReg27(Cinverse[1](0,0))); //missing pix1 constants
  out.push_back(floatToReg27(m_kaverage[secid][2])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][2][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =1;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[1](1,1))); //missing pix1 constants
  out.push_back(floatToReg27(Cinverse[1](0,1))); //missing pix1 constants
  out.push_back(floatToReg27(m_kaverage[secid][3])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][3][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[2](1,0))); //missing pix2 constants
  out.push_back(floatToReg27(Cinverse[2](0,0))); //missing pix2 constants
  out.push_back(floatToReg27(m_kaverage[secid][4])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][4][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[2](1,1))); //missing pix2 constants
  out.push_back(floatToReg27(Cinverse[2](0,1))); //missing pix2 consytants
  out.push_back(floatToReg27(m_kaverage[secid][5])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][5][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =2;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[3](1,0))); //missing pix3 constants
  out.push_back(floatToReg27(Cinverse[3](0,0))); //missing pix3 constants
  out.push_back(floatToReg27(m_kaverage[secid][6])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][6][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[3](1,1))); //missing pix3 constants
  out.push_back(floatToReg27(Cinverse[3](0,1))); //missing pix3 constants
  out.push_back(floatToReg27(m_kaverage[secid][7])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][7][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[4](0,0))); //missing sct4 constants
  out.push_back(floatToReg27(m_kaverage[secid][8])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][8][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =3;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[5](0,0))); //missing sct5 constants
  out.push_back(floatToReg27(m_kaverage[secid][9])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][9][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[6](0,0))); //missing sct6 constants
  out.push_back(floatToReg27(m_kaverage[secid][10])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(m_kernel[secid][10][ik2]/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[7](0,0))); //missing sct7 constants
  out.push_back(floatToReg27(m_fit_const[secid][1])); //d0
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (m_fit_pars[secid][1][ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =4;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[8](0,0))); //missing sct8 constants
  out.push_back(floatToReg27(m_fit_const[secid][3])); //z0
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (m_fit_pars[secid][3][ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  
  out.push_back(floatToReg27(Cinverse[9](0,0))); //missing sct9 constants 
  out.push_back(floatToReg27(m_fit_const[secid][4])); //coth
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (m_fit_pars[secid][4][ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[10](0,0))); //missing sct10 constants 
  out.push_back(floatToReg27(m_fit_const[secid][2])); //phi0
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (m_fit_pars[secid][2][ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =5;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[11](0,0))); //missing sct11 constants
  out.push_back(floatToReg27(m_fit_const[secid][0])); //curvature
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (m_fit_pars[secid][0][ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  for (int i=0; i<43;i++){
    out.push_back(0);
  }


  return;
}








/** This method prepares the inverted constants used in the invlinfit() */
void FTKConstantBank::prepareInvConstants()
{
//  m_invfit_consts = new TMatrixD[m_nsectors];
   m_invfit_consts = new std::vector<Eigen::MatrixXd>;
   m_invfit_consts->resize(m_nsectors);

   for (int isec=0;isec!=m_nsectors;++isec) { // loop over the sectors
      Eigen::MatrixXd thisMatrix(m_ncoords,m_ncoords);

      // first the parameters, by row
      for (int ip=0;ip!=m_npars;++ip) {
         for (int ix=0;ix!=m_ncoords;++ix) {
            thisMatrix(ip,ix)= m_fit_pars[isec][ip][ix];
         }
      }
      // The the constraints, by row
      for (int ic=0;ic!=m_nconstr;++ic) {
         for (int ix=0;ix!=m_ncoords;++ix) {
            thisMatrix(ic+m_npars,ix) = m_kernel[isec][ic][ix];
         }
      }
      
      //Eigen::FullPivLU<Eigen::MatrixXd> lu(thisMatrix);
      //cout<<"IsInvertible: "<<lu.isInvertible()<<endl;

      // Invert the matrix
      (*m_invfit_consts)[isec]= thisMatrix.inverse();

   } // end loop over the sectors
}


/** This method uses the track parameters and additional constraints to 
    use the constants to calculate the corresponding hits.
    This method is the base of the patt-from-constant generation. */

signed long long FTKConstantBank::aux_asr(signed long long input , int shift, int width , bool &overflow ) const {

  // deal with the sign separately
  unsigned long long shifted = abs(input);

  // make the baseline shift
  if (shift > 0) shifted = (shifted << shift);
  if (shift < 0) {
    // clear bits that will "go negative"
    shifted &= ~((static_cast<long long>(1) << -shift) - 1);
    shifted = (shifted >> -shift);
  }

  // save bits within the width (subtracting one bit for sign)
  signed long long output = shifted & ((static_cast<long long>(1) << (width-1))-1);
  if (static_cast<unsigned long long>(llabs(output)) != shifted) overflow = true;

  // reinstate the sign.
  if (input < 0) output *= -1;

  return output;

}

