#include <fstream>
#include <map>
#include <vector>
#include <memory>
#include <string>
#include <iostream>
#include <chrono>

#include <boost/program_options.hpp>

#include <FTKSector711DB.h>
#include <FTKConstantBank.h>



int main(int argc,char *argv[])
{
  const unsigned int completeNPlanes=12;
  const unsigned int nPlanes=8;
  const unsigned int nCoordsComplete=16;
  const unsigned int nCoordsIncomplete=11;

  boost::program_options::variables_map vm;
  std::string constantDir;
  std::string outputDir;
  unsigned int region;
  std::string regionStr;
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "")
      ("constantDir,c", boost::program_options::value<std::string>(&constantDir)->default_value("."), "constant dir")
      ("outputDir,o",  boost::program_options::value<std::string>(&outputDir)->default_value("."), "output dir")
      ("region,r", boost::program_options::value<unsigned int>(&region)->default_value(0), "region");
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      exit(0);
    }
  } 
  
  catch (const  boost::program_options::error &ex) {
    std::cerr << ex.what() << '\n';
  }

  regionStr=std::to_string(region);

  std::chrono::steady_clock::time_point read_start = std::chrono::steady_clock::now();
  
  std::string bank8File=constantDir+"/corrgen_raw_8L_reg"+regionStr+".gcon.bz2";
  std::string bank11File=constantDir+"/corrgen_raw_12L_reg"+regionStr+".gcon.bz2";
  std::string sectorFile=constantDir+"/sectors_raw_8L_reg"+regionStr+".conn";
  std::unique_ptr<FTKConstantBank>  bank8 ( new FTKConstantBank(nCoordsIncomplete,bank8File.c_str()));					    
  std::unique_ptr< FTKConstantBank> bank11( new  FTKConstantBank( nCoordsComplete, bank11File.c_str()));  
  std::unique_ptr<FTKSector711DB> sector(new FTKSector711DB(bank8->getNSectors(),completeNPlanes-nPlanes, sectorFile.c_str()));
  std::cout << "Threads used by Eigen: " << Eigen::nbThreads( ) << std::endl;

  std::chrono::steady_clock::time_point read_end = std::chrono::steady_clock::now();

  std::cout << "-> Parsed configuration files " 
	    << std::chrono::duration_cast<std::chrono::milliseconds>(read_end-read_start).count() << " ms " << std::endl;


  std::ofstream myfile;
  std::string expFile=outputDir+"/EXPConstants_reg"+regionStr+  ".txt";
  myfile.open (expFile.c_str());
  
  std::string tfFile=outputDir+"/TFConstants_reg"+regionStr +  ".txt";
  std::ofstream myfileTF;
  myfileTF.open (tfFile.c_str());

  std::vector<std::map <int, int> > vecOfMapSecID;
  std::vector<std::map <int, int> > vecOfMapNconn;
  std::chrono::steady_clock::time_point map_start = std::chrono::steady_clock::now();

  vecOfMapSecID.clear();
  vecOfMapNconn.clear();
  std::vector<std::vector<int>> moduleIDvec;
  moduleIDvec.clear();
 
  for(unsigned int isec=0;isec<bank8->getNSectors();isec++){
   for(unsigned int Nconn=0; Nconn< (int)sector->getNSimilarSectors(isec); Nconn++){
     if(Nconn >3 ) break;
     int TwelveLsecid = sector->getSimilarSecID(isec,Nconn);
     std::map<int,int> Map_secid_Nconn;
     Map_secid_Nconn[TwelveLsecid] = isec;
     std::map<int,int> Map_temp;
     Map_temp[TwelveLsecid] = (int)sector->getNSimilarSectors(isec) ;
     vecOfMapNconn.push_back(Map_temp);
     vecOfMapSecID.push_back(Map_secid_Nconn);
     std::vector<int> module_temp;
     module_temp.clear();
     for (int ip=0; ip!=(completeNPlanes-nPlanes); ++ip) {
       module_temp.push_back(sector->getSimilarStereoIDs(isec,Nconn)[ip]);
     }
     moduleIDvec.push_back(module_temp);
   }
  }
#if 0
  std::cout << "vecOfMapSecID\n";
  int k=0;
  for (auto &i:vecOfMapSecID) {for(auto &m: i) {std::cout << "["<<k<< "]"<< m.first << ": " << m.second  << std::endl;} k++;}
  std::cout << "vecOfMapNconn\n";
  k=0;
  for (auto &i:vecOfMapNconn) {for(auto &m: i) {std::cout << "["<<k<< "]" << m.first << ": " << m.second  << std::endl;}k++;}
#endif

  std::chrono::steady_clock::time_point map_end = std::chrono::steady_clock::now();
  std::cout << "-> Created connectivity map "
            << std::chrono::duration_cast<std::chrono::milliseconds>(map_end-map_start).count() << " ms " << std::endl;

  std::chrono::steady_clock::time_point calcTF_start = std::chrono::steady_clock::now();
  std::vector<uint32_t> out;

   for(unsigned int isec=0;isec<bank11->getNSectors();isec++){
       bank11->calcTFConstant(isec,out);
   }
       bank11->writeConstants(out,myfileTF);
 myfileTF.close();
 std::chrono::steady_clock::time_point calcTF_end = std::chrono::steady_clock::now();

std::cout << "-> Calculated TF constants"
            << std::chrono::duration_cast<std::chrono::milliseconds>(calcTF_end-calcTF_start).count() << " ms " << std::endl;

std::chrono::steady_clock::time_point calc_start = std::chrono::steady_clock::now();
 std::vector<uint32_t> out1;

  for(unsigned int isec=0;isec<bank11->getNSectors();isec++){
    //  bank11->calcTFConstant(isec,out);
    int temp = -9;
    int counter=-1;
    int index =-9;
    
    for(unsigned int Nconn=0; Nconn<(int)vecOfMapSecID.size(); Nconn++){
      std::map<int,int> &mymap= vecOfMapSecID[Nconn];
      std::map<int,int> &mymap_Nconn= vecOfMapNconn[Nconn];
      std::map<int,int>::iterator it;
      //for (std::map<int,int>::iterator it=mymap.begin(); it!=mymap.end(); ++it){
        it=mymap.begin();
	auto el= mymap_Nconn.begin();  //mymap_Nconn.find(isec);
	//	if(el== mymap_Nconn.end()) continue;
	unsigned int count_Nconn = el->second ;
	//	std::cout << "isec= "<<isec << ", Nconn= " << Nconn << ", count_Nconn= "<< count_Nconn << ", size=" <<mymap.size() <<std::endl; 
	if(count_Nconn >4)count_Nconn =4;
	if(it->second != temp) index=-1;
	if(index <0){
	  temp  = it->second;
	  index = 1;
	  counter =-1;
	}
	if(it->second == temp && index>=0)    counter++;
	
       if (it->first == isec){
	 	 bank11->calcExtrapolationConstant(isec,moduleIDvec[Nconn],it->second,counter,count_Nconn, out1);
		 //		 std::cout << "ex isec="<<isec<<", second="<<it->second<<std::endl;

	 break;
       }
       //}
    }
  }
    bank11->writeConstants(out1,myfile);

  myfile.close();
  
  std::chrono::steady_clock::time_point calc_end = std::chrono::steady_clock::now();
  std::cout << "-> Calcated SSB constants "
            << std::chrono::duration_cast<std::chrono::milliseconds>(calc_end-calc_start).count() << " ms " << std::endl;

  return 0;
}
